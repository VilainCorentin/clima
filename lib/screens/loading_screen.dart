import 'package:clima/services/location.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  void initState() {
    getPosition();
    getData();
    super.initState();
  }

  void getPosition() async {
    Location location = Location();
    await location.getCurrentLocation();
    print('Longitude : ' +
        location.longitude.toString() +
        ' Latitude : ' +
        location.latitude.toString());
  }

  void getData() async {
    Response response = await get(
        'https://samples.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=b6907d289e10d714a6e88b30761fae22');

    if (response.statusCode == 200) {
      var decodedData = jsonDecode(response.body);

      double temp = decodedData['main']['temp'];
      int weatherId = decodedData['weather'][0]['id'];
      String name = decodedData['name'];

      print(temp);
      print(weatherId);
      print(name);
    } else {
      print('ERROR RESPONSE CODE : ' + response.statusCode.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
